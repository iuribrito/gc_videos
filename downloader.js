const fs = require('fs');
const path = require('path');
const axios = require('axios');
const ProgressBar = require('progress');

async function get (url, filename) {
    console.log('Iniciando Download: ' + filename);
    const localPath = path.resolve(__dirname, 'videos', filename)

    const response = await axios({
        method: 'GET',
        url: url,
        responseType: 'stream'
    });

    response.data.pipe(fs.createWriteStream(localPath));

    const totalBytes = parseInt(response.data.headers['content-length'], 10);
    // let receivedBytes = 0;

    const bar = new ProgressBar('downloading [:bar] :rate/bps :percent :etas', {
        complete: '█',
        incomplete: '░',
        width: 50,
        total: totalBytes
    });

    response.data.on('data', data => {
        bar.tick(data.length);
        // receivedBytes += data.length;
        // let percent = (receivedBytes * 100) /  totalBytes;
        // console.info('=> ', (receivedBytes/1048576).toFixed(2) + ' MB - ' + percent.toFixed() + '%')
    });

    return new Promise((resolve, reject) => {
        response.data.on('end', () => resolve());
        response.data.on('error', () => reject());
    })
}

module.exports.get = get;
