const puppeteer = require('puppeteer');
const config = require('./config');
const downloader = require('./downloader');

(async () => {
    const browser = await puppeteer.launch({executablePath: config.browserPath, headless: false});
    const pages = await browser.pages();
    const page = pages[0];

    console.info('Iniciando ...');

    await page.goto('https://gamersclub.com.br/auth');
    page.click('.col-sm-4.small-left-padding .auth-btn');
    await page.waitForNavigation();

    await page.type('#steamAccountName', config.username);
    await page.type('#steamPassword', config.password);

    page.click('#imageLogin');

    console.info('Inserir Codigo de Autenticacao:');

    for(let i = config.time; i > 0 ; i--) {
        console.log(i + ' segundos')
        await page.waitFor(1000);
    }

    await page.goto(config.lessonUrl);
    await page.waitFor(2000);
    // await page.waitForNavigation();

    const linkCollection = await page.evaluate(() => {
         let response = [];
         const links = document.querySelectorAll('.GaSerieEpisodesItem');

         for (const link of links) {
            const linkName = link.querySelector('.GaSerieEpisodesItem__title h4').textContent.trim().replace(/[\.\-:()\/+=,]/g, '').replace(/\s+/g, '_').toLowerCase();
            response.push({url: link.href, name: linkName + '.mp4'});
         }

         return response;
    });

    let loopCount = 0;

    for (const linkPage of linkCollection) {
        console.info('Aula: ' + linkPage.url);
        loopCount++;
        if (config.offset > 0 && loopCount < config.offset) {
            console.info('Skip');
            continue;
        }
        const pageTab = await browser.newPage();
        await pageTab.goto(linkPage.url);

        await page.waitFor(5000);

        const frames = await pageTab.frames();
        const vimeoFrame = await frames.find(frame => frame.url().includes('vimeo'));
        const contentFrame = await vimeoFrame.content();

        const partner1 = contentFrame.match(/{.*?720p.*?}/g)
        const partner2 = partner1[2].match(/"https:.*?"/g)
        const urlVideo = partner2[(partner2.length - 1)];

        await downloader.get(urlVideo.replace(/\"/g, ''), linkPage.name)
        .then(response => console.info('Download Finalizado!'))
        .catch(err => console.error(err.message));

        await pageTab.close();
    }

    console.log('Parabens! voce baixou todas as aulas seu cabra safado.')
    await browser.close();
})();
